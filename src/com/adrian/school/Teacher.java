package com.adrian.school;

// Extends answers tha following "Teacher (is a) Person"
public class Teacher extends Person {

    private String subject;

    // Declarare constructor fara parametrii
    public Teacher(){
        super();
    }

    // Declarare constructor cu parametrii
    public Teacher(String firstName, String lastName, Long cnp, int age, String subject){
        super(firstName, lastName, cnp, age);
        this.subject = subject;
    }

    //************** GETTER ********************

    public String getSubject(){
        return this.subject;
    }

    //************** SETTER ********************


    public void setSubject(String subject){
        this.subject = subject;
    }

    @Override
    public String toString() {
        return super.toString() + " si sunt profesor de " + subject;
    }
}
