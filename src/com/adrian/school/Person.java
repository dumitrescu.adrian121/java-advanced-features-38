package com.adrian.school;

public class Person {

    private String firstName;
    private String lastName;
    private Long cnp;
    private int age;

    public Person() {

    }

    public Person(String firstName, String lastName, Long cnp, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.cnp = cnp;
        this.age = age;
    }

    //************** GETTER ********************

    public String getFirstName(){
        return this.firstName;
    }

    public String getLastName(){
        return this.lastName;
    }

    public int getAge(){
        return this.age;
    }

    public Long getCNP(){
        return this.cnp;
    }

    //************** SETTER *******************

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public void setAge(int age){
        this.age = age;
    }

    //@Override
    public String toString() {
        //Salut! Eu sunt Ana Pop, am 44 ani
        return "Salut! Ma numesc " + firstName +
                " " + lastName + ", am " + age + " de ani ";

    }
}

