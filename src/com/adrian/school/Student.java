package com.adrian.school;


import java.security.PublicKey;

public class Student extends  Person{

    private double examGrade;

    // Declarare constructor fara parametrii
    public Student(){

    }

    // Declarare constructor cu parametrii
    public Student(String firstName, String lastName, Long cnp, int age, double examGrade){
        // super apeleaza constructorul SUPERclasei Person
        super(firstName,lastName,cnp,age);
        this.examGrade = examGrade;
    }

    //************** GETTER ********************

    public double getExamGrade(){
        return this.examGrade;
    }


    //************** SETTER ********************

    public void setExamGrade(double examGrade){
        this.examGrade = examGrade;
    }

    @Override
    public String toString() {
        return super.toString() + "si am absolvit cu nota " + examGrade;
    }
}
