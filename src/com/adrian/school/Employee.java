package com.adrian.school;

// Extends answers tha following "Teacher (is a) Person"
public class Employee extends Person {

    private String occupation;

    // Declarare constructor fara parametrii
    public Employee(){
        // Super este apelat automat, nu trebuie scris
        super();
    }

    // Declarare constructor cu parametrii
    public Employee(String firstName, String lastName, Long cnp, int age, String occupation){
        super(firstName, lastName, cnp, age);
        this.occupation = occupation;
    }

    //************** GETTER ********************

    public String getSubject(){
        return this.occupation;
    }

    //************** SETTER ********************


    public void setSubject(String occupation){
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return super.toString() + " si sunt " + occupation;
    }
}
