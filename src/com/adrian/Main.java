package com.adrian;

import com.adrian.school.Employee;
import com.adrian.school.Student;
import com.adrian.school.Teacher;
import com.adrian.shapes.Rectangle;
import com.adrian.shapes.Square;
import com.adrian.shapes.Triangle;

public class Main {

    public static void main(String[] args) {
	// write your code here

        // ********************** SHAPES *******************************
//        Student s1 = new Student("Adrian", "Dumitrescu", 12312515l, 28, 8.6);
//        Teacher t1 = new Teacher("Maria", "Popescu", 125123524l, 45, "Biologie");
//        Employee e1 = new Employee("Marian", "Tanase", 1251224l, 30, "Java Developer");
//
//        System.out.println(s1.toString());
//        System.out.println(t1.toString());
//        System.out.println(e1.toString());

        // ********************** SHAPES *******************************

        Rectangle r1 = new Rectangle(10D, 5D, "Blue");
        Triangle t1 = new Triangle(10D, 5D, 5D, "Red");
        Square s1 = new Square(10D, "Black");

        System.out.println(r1.toString());
        System.out.println();
        System.out.println(t1.toString());
        System.out.println();
        System.out.println(s1.toString());

//        System.out.println("The area of the Rectangle is: " + r1.getArea());
//        System.out.println("The area of the Triangle is: " + t1.getArea());
//        System.out.println("The area of the Square is: " + s1.getArea());



    }
}


