package com.adrian.shapes;

public class Triangle extends Shapes{

    private Double height;

    public Triangle(){
        super();
    }

    public Triangle(Double length, Double width, Double height) {
        super(length, width);
        this.height = height;
    }

    public Triangle(Double length, Double width, Double height, String colour) {
        super(length, width, colour);
        this.height = height;
    }

    //************** GETTER ********************

    public Double getHeight() {
        return height;
    }

    //************** SETTER *******************
    public void setHeight(Double height) {
        this.height = height;
    }

    @Override
    public Double getArea(){
        return (getLength()*height) / 2;
    }

    @Override
    public String toString() {
        return "This is a " + getClass().getSimpleName() + " and it's parameters are: \n" +
                "\nHeight: " + height +
                super.toString() +
                "\nArea: " + getArea();
    }
}
