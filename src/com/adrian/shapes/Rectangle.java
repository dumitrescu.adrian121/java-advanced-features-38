package com.adrian.shapes;

public class Rectangle extends Shapes{

    public Rectangle(){
        super();
    }

    public Rectangle(Double length, Double width) {
        super(length, width);
    }

    public Rectangle(Double length, Double width,String colour) {
        super(length, width,colour);
    }

    @Override
    public Double getArea(){
        return getLength()*getWidth();
    }

    @Override
    public String toString() {
        return "This is a " + getClass().getSimpleName() + " and it's parameters are: \n" +
                super.toString() +
                "\nArea: " + getArea();
    }


}
