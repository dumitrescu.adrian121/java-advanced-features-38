package com.adrian.shapes;

public class Shapes {

    // Shape length __
    // Shape width  |
    private Double length; // base
    private Double width;
    private String colour;

    public Shapes() {

    }

    public Shapes(Double length) {
        this.length = length;
    }

    public Shapes(Double length, String colour) {
        this.length = length;
        this.colour = colour;
    }

    public Shapes(Double length, Double width) {
        this.length = length;
        this.width = width;
    }

    public Shapes(Double length, Double width, String colour) {
        this.length = length;
        this.width = width;
        this.colour = colour;
    }

    //************** GETTER ********************

    public Double getLength() {
        return length;
    }

    public Double getWidth() {
        return width;
    }

    public String getColour() {
        return colour;
    }


    public Double getArea() {
        return null;
    }

    //************** SETTER *******************

    public void setLength(Double length) {
        this.length = length;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        String concatString = null;
        if(getLength()!=null){
            stringBuilder.append("\nLength: " + getLength());
        }
        if(getWidth()!=null){
            stringBuilder.append("\nWidth: " + getWidth());
        }
        if(getColour()!=null){
            stringBuilder.append("\nColour: " + getColour());
        }
        concatString = stringBuilder.toString();
        return  concatString;

    }

}
