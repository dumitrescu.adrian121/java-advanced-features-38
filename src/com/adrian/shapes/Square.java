package com.adrian.shapes;

public class Square extends Shapes{

    public Square(){
        super();
    }

    public Square(Double length) {
        super(length);
    }

    public Square(Double length, String colour) {
        super(length,colour);
    }

    @Override
    public Double getArea(){
        return getLength()*getLength();
    }

    @Override
    public String toString() {
        return "This is a " + getClass().getSimpleName() + " and it's parameters are: \n" +
                super.toString() +
                "\nArea: " + getArea();
    }


}
